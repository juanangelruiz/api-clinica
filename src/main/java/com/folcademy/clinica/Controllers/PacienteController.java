package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {

        this.pacienteService = pacienteService;
    }


    @GetMapping("")
    public ResponseEntity<List<PacienteDto>> findAll() {
        return ResponseEntity
                .ok()
                .body(pacienteService.findAll());
    }

    @GetMapping("/page")
    public ResponseEntity<Page<PacienteDto>> findAllPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name= "orderField", defaultValue = "apellido") String orderField
    ) {
        return ResponseEntity
                .ok()
                .body(pacienteService.findAllByPage(pageNumber,pageSize,orderField));
    }

    @GetMapping("/{idPaciente}")
    public ResponseEntity<PacienteDto> findAll(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity
                .ok()
                .body(pacienteService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<PacienteDto> create(@RequestBody @Validated PacienteDto dto) {
        return ResponseEntity
                .ok()
                .body(pacienteService.create(dto));
    }

    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDto> edit (@PathVariable(name = "idPaciente") int id,
                                             @RequestBody @Validated PacienteDto dto) {
        return ResponseEntity
                .ok()
                .body(pacienteService.edit(id, dto));
    }

    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> delete (@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity
                .ok()
                .body(pacienteService.delete(id));
    }
}
