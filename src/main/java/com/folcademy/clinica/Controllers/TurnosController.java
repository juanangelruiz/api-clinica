package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turnos")
public class TurnosController {
    private final TurnoService turnoService;

    public TurnosController(TurnoService turnoService) {

        this.turnoService = turnoService;
    }

    @GetMapping("")
    public ResponseEntity<List<TurnoDto>> findAll(){

        return ResponseEntity
                .ok()
                .body(turnoService.findAll());
    }

    @GetMapping("/page")
    public ResponseEntity<Page<TurnoDto>> findAllPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name= "orderField", defaultValue = "idturno") String orderField
    ){
        return ResponseEntity
                .ok()
                .body(turnoService.findAllByPage(pageNumber,pageSize,orderField));
    }

    @GetMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> findById(@PathVariable(name = "idTurno") int id){
        return ResponseEntity
                .ok()
                .body(turnoService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<TurnoDto> create(@RequestBody @Validated TurnoDto dto){
        return ResponseEntity
                .ok()
                .body(turnoService.create(dto));
    }

    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> edit (@PathVariable(name = "idTurno") int id,
                                          @RequestBody @Validated TurnoDto dto) {
        return ResponseEntity
                .ok()
                .body(turnoService.edit(id, dto));
    }

    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> delete (@PathVariable(name = "idTurno") int id) {
        return ResponseEntity
                .ok()
                .body(turnoService.delete(id));
    }
}
