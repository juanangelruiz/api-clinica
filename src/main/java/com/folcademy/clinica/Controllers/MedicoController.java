package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {

        this.medicoService = medicoService;
    }

    @GetMapping("")
    public ResponseEntity<List<MedicoDto>> findAll(
    ){
        return ResponseEntity
                .ok()
                .body(medicoService.findAll());
    }

    @GetMapping("/page")
    public ResponseEntity<Page<MedicoDto>> findAllPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name= "orderField", defaultValue = "apellido") String orderField
    ){
        return ResponseEntity
                .ok()
                .body(medicoService.findAllByPage(pageNumber,pageSize,orderField));
    }

    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> findById(@PathVariable(name = "idMedico") int id){
        return ResponseEntity
                .ok()
                .body(medicoService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<MedicoDto> create(@RequestBody @Validated MedicoDto dto){
        return ResponseEntity
                .ok()
                .body(medicoService.create(dto));
    }

    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> edit (@PathVariable(name = "idMedico") int id,
                                           @RequestBody @Validated MedicoDto dto) {
        return ResponseEntity
                .ok()
                .body(medicoService.edit(id, dto));
    }

    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> delete (@PathVariable(name = "idMedico") int id) {
        return ResponseEntity
                .ok()
                .body(medicoService.delete(id));
    }

}