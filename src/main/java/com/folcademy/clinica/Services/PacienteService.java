package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService{
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    public List<PacienteDto> findAll() {
        List<Paciente> pacientes = (List<Paciente>) pacienteRepository.findAll();
        return pacientes.stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }

    public Page<PacienteDto> findAllByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }
    public PacienteDto findById(Integer id) {
        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElseThrow(() -> new NotFoundException("Paciente inexistente"));
    }

    public PacienteDto create(PacienteDto dto) {
        if(!dto.getDni().matches("\\d+"))
            throw new BadRequestException("El DNI debe contener solo números");
        dto.setId(null);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(dto)));
    }

    public PacienteDto edit(Integer id, PacienteDto dto) {
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("Paciente inexistente");
        if(!dto.getDni().matches("\\d+"))
            throw new BadRequestException("El DNI debe contener solo números");
        dto.setId(id);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(dto)));
    }

    public Boolean delete(Integer id) {
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("Paciente inexistente");
        pacienteRepository.deleteById(id);
        return true;
    }
}
