package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    public MedicoDto entityToDto(Medico entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getId(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getProfesion(),
                                ent.getConsulta()
                        )
                )
                .orElse(new MedicoDto());
    }
    public Medico dtoToEntity(MedicoDto dto) {
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        return entity;
    }
}

