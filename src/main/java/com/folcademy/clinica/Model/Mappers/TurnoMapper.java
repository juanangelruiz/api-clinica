package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
    public TurnoDto entityToDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getIdturno(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico(),
                                ent.getPaciente(),
                                ent.getMedico()
                        )
                )
                .orElse(new TurnoDto());
    }
    public Turno dtoToEntity(TurnoDto dto) {
        Turno entity = new Turno();
        entity.setIdturno(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        entity.setPaciente(dto.getPaciente());
        entity.setMedico(dto.getMedico());
        return entity;
    }
}

