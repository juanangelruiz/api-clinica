package com.folcademy.clinica.Model.Dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDto {
    public Integer id;
    @NotNull
    public String nombre;
    @NotNull
    public String apellido;
    @NotNull
    public String profesion;
    public int consulta;
}
